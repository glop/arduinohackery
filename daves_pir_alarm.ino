/*

   Dave's PIR Alarm.. WHEEEEEEE
   Code cobbled together from various places, and modified to suit as needed.

   This basically requires the following:

   (1) An Arduino
   (2) A PIR sensor (I have a Paralax, from Radio Shack ~$10
   (3) A wired circuit
   (4) This madness..

 */

// hardware config
int ledPin = 13;                // choose the pin for the LED
int inputPin = 2;               // choose the input pin (for PIR sensor)
int pirState = LOW;             // we start, assuming no motion detected
int val = 0;                    // variable for reading the pin status
int speakerPin = 9;

// sound config
int beats = 1;
int tempo = 30;
 
void setup() {
  pinMode(ledPin, OUTPUT);
  pinMode(inputPin, INPUT);
  pinMode(speakerPin, OUTPUT);

  // debug cruft (use Control-shift-M to view)
  Serial.begin(9600);
  Serial.println("PIR watching..");
  Serial.println(digitalRead(inputPin));
}

 
void loop() {
  val = digitalRead(inputPin);  // read input value
  if (val == HIGH) {
    digitalWrite(ledPin, HIGH);  // turn LED on
    playAlarm(1, beats * tempo);  // tone, duration
  
    if (pirState == LOW) {
      
      // we have just turned on
      Serial.println("Motion detect.");
      pirState = HIGH;
    }
  } else {
    digitalWrite(ledPin, LOW); // turn LED off
    if (pirState == HIGH){     // motion ends  
      Serial.println("Motion stopped.");
      pirState = LOW;
    }
  }
} 

// interprets the frequencies to give us something that sounds variable
void playTone(int tone, int duration) {
  for (long i = 0; i < duration * 1000L; i += tone * 2) {
    digitalWrite(speakerPin, HIGH);
    delayMicroseconds(tone);
    digitalWrite(speakerPin, LOW);
    delayMicroseconds(tone);    
  }
}

void playAlarm(char note, int duration) {
  // just some frequencies that sounded like an alarm; via experiements ;)
  int tones[] = { 3000, 1600, 3000, 1600, 3000, 2000, 2100, 2200 };
  
  for (int i = 0; i < 8; i++) {
      playTone(tones[i], duration);
  }
}
