/*
 
 Most of network code by David Mellis. See the Arduino.cc site for more info.

 Adapted for syslog and other stuff by yours truly. This is all pretty much buggy and crappy code. Just FYI.
 
 Taking this a bit further, set this up.
 
 - Connect to your laptop over crossover. Tail your local /var/log/syslog to see what the Arduino gets as an IP address.
 - Set up a nc listener, and forward to syslog.
 
 # nc -vv -l 514 | logger
 
 - Tail your syslog
$ tail -f /var/log/syslog

 A better setup would be to just uncomment the rsyslog.conf lines to allow remote syslog.
 Otherwise, your nc will need to be restarted after a connection is made.
 
 */

#include <SPI.h>
#include <Ethernet.h>


// hardware config
int ledPin = 8;                // choose the pin for the LED
int inputPin = 2;               // choose the input pin (for PIR sensor)
int pirState = LOW;             // we start, assuming no motion detected
int val = 0;                    // variable for reading the pin status
int speakerPin = 9;

// sound config
int beats = 1;
int tempo = 30;

// my syslog port
int port = 514;

// Should match the Arduino's eth MAC
byte mac[] = {  0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };  // get your own MAC here

// define an IP address.. shouldn't need this for this example
//IPAddress ip(10,42,43,2);

// host to connect to, address of your local machine which is crossover connected
IPAddress server(10,42,43,1)
;

// Initialize the Ethernet client library
EthernetClient client;

void setup() {
  
  pinMode(ledPin, OUTPUT);
  pinMode(inputPin, INPUT);
  pinMode(speakerPin, OUTPUT);

  // debug cruft (use Control-shift-M to view)
  Serial.begin(9600);
  Serial.println("PIR watching..");
  Serial.println(digitalRead(inputPin));
  
  // start the serial library:
  //Serial.begin(9600);
  // start the Ethernet connection:
  //Ethernet.begin(mac, ip);
  
  if (Ethernet.begin(mac) == 0) {
    Serial.println("Failed to configure Ethernet using DHCP");
    // no point in carrying on, so do nothing forevermore:
    for(;;)
    
      ;
  }
  
  Serial.print("Using IP address: ");
  Serial.println(Ethernet.localIP());
 
}

void loop()
{
  val = digitalRead(inputPin);  // read input value
  if (val == HIGH) {
    
    digitalWrite(ledPin, HIGH);  // turn LED on
    playAlarm(1, beats * tempo);  // tone, duration    
    
    if (pirState == LOW) {
      // we have just turned on
      Serial.println("Motion detect.");
      pirState = HIGH;  
      logIntrusion(); // send to syslog server >:D
    }
  } else {
    digitalWrite(ledPin, LOW); // turn LED off
    if (pirState == HIGH){     // motion ends  
      Serial.println("Motion stopped.");
      pirState = LOW;
      digitalWrite(ledPin, LOW);
      
    }
  }  
}

void logIntrusion() {  
  // give the Ethernet shield a second to initialize:
  delay(1000);
  Serial.println("Connecting to server..");

  // if you get a connection, report back via serial:
  
  // my test to send a request to syslog over a nc listener on 9514
  if (client.connect(server, port)) {
    Serial.println("Logging message..");
    // Make a HTTP request:
    client.println("*** INTRUSION DETECTED! ***");
    client.println();
  }
  else {
    // kf you didn't get a connection to the server:
    Serial.println("Failed to connect to server.");
  }
  
    // if there are incoming bytes available
    
  // from the server, read them and print them:
  /*if (client.available()) {
    
    char c = client.read();
    Serial.print(c);
  }*/

  // if the server's disconnected, stop the client:
  if (!client.connected()) {
    Serial.println();
    Serial.println("Disconnecting.");
    client.stop();

    // do nothing forevermore:
    for(;;)
      ;
      
  }
}

// interprets the frequencies to give us something that sounds variable
void playTone(int tone, int duration) {
  for (long i = 0; i < duration * 1000L; i += tone * 2) {
    digitalWrite(speakerPin, HIGH);
    delayMicroseconds(tone);
    digitalWrite(speakerPin, LOW);
    delayMicroseconds(tone);    
  }
}

void playAlarm(char note, int duration) {
  // just some frequencies that sounded like an alarm; via experiements ;)
  int tones[] = { 3000, 1600, 3000, 1600, 3000, 2000, 2100, 2200 };
  
  for (int i = 0; i < 8; i++) {
      playTone(tones[i], duration);
  }
}
