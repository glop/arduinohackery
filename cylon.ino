/*

 A couple of for loops for a cylon simulation. This is probably buggy, but should work for an example.

*/
int ledPins[] = {2,3,4,5,6,7,8,9};

void setup() {
 for (int i=0; i < 8; i++) {
   pinMode(ledPins[i],OUTPUT);
 }
}

void loop() {
  ledLoop(); 
}

void ledLoop() {
 int delayTime = 20;
 
 for (int j = 0; j < 8; j++) {
  digitalWrite(ledPins[j],HIGH);
  delay(delayTime);
  digitalWrite(ledPins[j],LOW);
  delay(delayTime); 
 }
 
 for (int k = 8; k >= 0; k--) {
  digitalWrite(ledPins[k],HIGH);
  delay(delayTime);
  digitalWrite(ledPins[k],LOW);
  delay(delayTime); 
 } 
 
}
