/*
  
 Original code by David A. Mellis. Modified for syslog.
 Just a little test to see if we can get syslog working. Need an eth shield for an Arduino.

 
 */

#include <SPI.h>
#include <Ethernet.h>

// Enter a MAC address for your controller below.
// Newer Ethernet shields have a MAC address printed on a sticker on the shield
byte mac[] = {  0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }; // need to add your own MAC here..

// define an IP address
//IPAddress ip(10,42,43,2);

// host to connect to
IPAddress server(10,42,43,1); // inside

// Initialize the Ethernet client library
// with the IP address and port of the server
// that you want to connect to (port 80 is default for HTTP):
EthernetClient client;

void setup() {
  // start the serial library:
  Serial.begin(9600);
  // start the Ethernet connection:
  //Ethernet.begin(mac, ip);
  
  if (Ethernet.begin(mac) == 0) {
    Serial.println("Failed to configure Ethernet using DHCP");
    // no point in carrying on, so do nothing forevermore:
    for(;;)
    
      ;
  }
  
  Serial.print("Using IP address: ");
  Serial.println(Ethernet.localIP());
  
  // give the Ethernet shield a second to initialize:
  delay(1000);
  Serial.println("connecting...");

  // if you get a connection, report back via serial:
  // my test to send a request to syslog over a nc listener on 9514
  if (client.connect(server, 9514)) {
    Serial.println("connected");
    // Make a HTTP request:
    client.println("HELLO WORLD from the Arduino!\n");
    client.println();
  }
  else {
    // kf you didn't get a connection to the server:
    Serial.println("connection failed");
  }
}

void loop()
{
  // if there are incoming bytes available
  // from the server, read them and print them:
  if (client.available()) {
    char c = client.read();
    Serial.print(c);
  }

  // if the server's disconnected, stop the client:
  if (!client.connected()) {
    Serial.println();
    Serial.println("disconnecting.");
    client.stop();

    // do nothing forevermore:
    for(;;)
      ;
  }
}
